import React from 'react'
import Axios from 'axios'
import GamesForm from './GamesForm'

export default class Games extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            games: [],
            selecionado: null
        }
    }

    componentDidMount = () => {
        this.handleGet()
    }

    handleGet = () => {
        const url = 'http://localhost:3001/api/game'
        var request = Axios.get(url)

        request.then((response) => {
            console.log(response.data)

            this.setState({
                games: response.data
            })
        })
    }

    handleSubmit = (game) => {
        if(this.state.selecionado == null){
            this.handlePost(game)
        } else {
            this.handlePut(this.state.selecionado, game)
        }
    }

    handlePost = (game) => {

        this.setState({
            games: [...this.state.games, game]
        })

        const url = 'http://localhost:3001/api/game'
        var request = Axios.post(url, game)

        request.then((response) => {
            console.log(response.data)
        })
    }

    handlePut = (id, game) => {

        this.setState({
            selecionado: null
        })

        const url = 'http://localhost:3001/api/game/' + id
        var request = Axios.put(url, game)

        request.then((response) => {
            this.handleGet()
        })
    }

    handleDelete = (id) => {
        console.log("Deletar" + id)

        const url = 'http://localhost:3001/api/game/' + id
        var request = Axios.delete(url)

        request.then((response) => {
            this.handleGet()
        })
    }

    handleSelect = (id) => {
        console.log("Indice: " + id)
        this.setState({
            selecionado: id
        })
    }

    render(){

        var listaGames = this.state.games.map((item, indice) => {
            return <div key={indice}>{item.nome} ({item.ano}) 
                <button onClick={() => this.handleDelete(indice)}>Deletar</button>
                <button onClick={() => this.handleSelect(indice)}>Selecionar</button>
            </div>
        })

        return <div>
        <h1>Lista de Jogos</h1>
            <GamesForm handlePost={this.handleSubmit}></GamesForm>
            <section>
                {listaGames}
            </section>
        </div>
    }
}