import React from 'react'

export default class GamesForm extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "",
            ano: "",
            estilo: "",
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const game = {
            nome: this.state.nome,
            estilo: this.state.estilo,
            ano: this.state.ano
        }

        this.props.handlePost(game)
    }

    handleChange = (event) => {
        event.preventDefault()

        this.setState({
            [event.target.id]: event.target.value
        })
    }

    render(){
        return <form onSubmit={this.handleSubmit}>
            <input type="text" id="nome" placeholder="Nome" value={this.state.nome} onChange={this.handleChange}></input>
            <input type="text" id="ano" placeholder="Ano" value={this.state.ano} onChange={this.handleChange}></input>
            <input type="text" id="estilo" placeholder="Estilo" value={this.state.estilo} onChange={this.handleChange}></input>
            <button type="submit">Cadastrar</button>
        </form>
    }
}